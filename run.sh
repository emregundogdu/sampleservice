#!/bin/bash

set -e
set +xv ## Do not remove

info_n() { echo -e "\e[36m$@\e[0m" 1>&2 ; }
info() { echo "" ; info_n $* ; }
warn() { echo ""; echo -e "\e[33m$@\e[0m" 1>&2; }
die() { echo -e "\e[31m$@\e[0m" 1>&2 ; exit 1; }
JOBHOSTNAME=`hostname`

export BLOB_POSTFIX=infra
export CONTAINER="tfstate-${account_name_prefix}-${service_name}-$BLOB_POSTFIX-$Environment"
export CONTAINER_JAR="${service_name}-artifactory"
export ARTIFACT_DIR=${artifact_dir}
export STATIC_RG="mdsp-${account_name_prefix}-$Environment"
export MICRO_SERVICE=${MICRO_SERVICE}
export AKS_DEPLOY_ID=${Environment}_AKS_DEPLOY_ID
export AKS_DEPLOY_SECRET=${Environment}_AKS_DEPLOY_SECRET


export IMAGE_TAG=${IMAGE_TAG}
echo "-------IMAGE TAG INFO-----------"
echo $IMAGE_TAG


export AZURE_STORAGE_CONTAINER_NAME="$(echo "${CI_PROJECT_NAME}" | tr '[:upper:]' '[:lower:]')-artifactory"
export RESOURCEGROUP="mdsp-${Region}-${account_name_prefix}-rg-$Environment"
export AZURE_STORAGE_ACCOUNT="mdsp${Region}${account_name_prefix}st${Environment}"

export client_id=$(eval echo "\$$(echo "${Environment}"_client_id)")
export client_secret=$(eval echo "\$$(echo "${Environment}"_client_secret)")
export tenant_id=$(eval echo "\$$(echo "${Environment}"_tenant_id)")
export subscription_id=$(eval echo "\$$(echo "${Environment}"_subscription_id)")
export aks_namespace=${NAMESPACE}

function configureAz(){
      info "Configuring Az..."
      az login --service-principal -u ${client_id} -p ${client_secret} --tenant ${tenant_id}
      az account set --subscription $subscription_id
}

function azurek8sLogin(){
      info "Login Az..."
      az login --service-principal -u ${!AKS_DEPLOY_ID} -p ${!AKS_DEPLOY_SECRET} --tenant ${tenant_id}
      az account set --subscription $subscription_id
}

function getk8screds(){
     rm -rf ~/.kube
     echo "Get Kubernetes credentials"
     az aks get-credentials --resource-group mdsp-${account_name_prefix}-${Environment} --name mdsp-${account_name_prefix}-${Environment}-aks --admin &> /dev/null
}

function logoutAz() {
   echo "INFO - Logout from azure"
   az logout
}


function run(){
    duty=$1
    pwd

    case $duty in
      "helm_plan")
        azurek8sLogin
        getk8screds
        release=$(HELM_NAMESPACE=$aks_namespace helm ls  --deployed | grep ${RELEASENAAME} | wc -l)
        if [[ $release -eq 0 ]]; then
        helm install ${RELEASENAAME} ${RELEASENAAME}/  -f ${RELEASENAAME}/values.yaml -f ${RELEASENAAME}/values.${Environment}.yaml --namespace $aks_namespace --debug --dry-run --timeout 10m
        else
        echo "--- Helm Release Exists---"
        helm ls --namespace=$aks_namespace
        HELM_NAMESPACE=$aks_namespace  helm diff upgrade ${RELEASENAAME} ${RELEASENAAME}/ -f  ${RELEASENAAME}/values.yaml -f ${RELEASENAAME}/values.${Environment}.yaml --namespace $aks_namespace

        fi
        logoutAz
      ;;

      "helm_install")
        azurek8sLogin
        getk8screds
          # This job is for k8s RBAC

          helm upgrade --install ${RELEASENAAME} ${RELEASENAAME}/ -f ${RELEASENAAME}/values.yaml -f ${RELEASENAAME}/values.${Environment}.yaml   --namespace $aks_namespace --debug --wait --timeout 10m
          logoutAz
      ;;

      "helm_uninstall")
       azurek8sLogin
       getk8screds
       HELM_NAMESPACE=$aks_namespace helm uninstall ${RELEASENAAME}
       logoutAz
      ;;
      "helm_rollback")

        azurek8sLogin
        getk8screds
        HELM_NAMESPACE=$aks_namespace helm rollback ${RELEASENAAME}
        logoutAz
      ;;

    *)
    ;;
    esac
}

info_n "#################################### Variable Details ##################################"

info_n "REPONAME                  = $service_name"
info_n "RESOURCEGROUP             = $RESOURCEGROUP"
info_n "SUBSCRIPTION_ID           = $subscription_id"
info_n "Branch_NAME               = $CI_COMMIT_REF_NAME"
info_n "JOBHOSTNAME               = $JOBHOSTNAME"
info_n "Environment               = $Environment"
info_n "Namespace"                = $aks_namespace

info "########################################################################################"

run $1