{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "sampleservice.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/* If we want to pass  HPA new style this able to use

{{- define "sampleservice.hpatype" -}}
{{- default "Resource" .Values.hpa.typeOverride -}}
{{- end -}}
*/}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "sampleservice.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "sampleservice.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "sampleservice.image" -}}
{{- $registryName := .Values.image.registry -}}
{{- $repositoryName := .Values.image.repository -}}
{{- $tag := .Values.image.tag | toString -}}

{{/*
Helm 2.11 supports the assignment of a value to a variable defined in a different scope,
but Helm 2.9 and 2.10 doesn't support it, so we need to implement this if-else logic.
Also, we can't use a single if because lazy evaluation is not an option
*/}}
{{- if .Values.global }}
    {{- if .Values.global.imageRegistry }}
        {{- printf "%s/%s:%s" .Values.global.imageRegistry $repositoryName $tag -}}
    {{- else -}}
        {{- printf "%s/%s:%s" $registryName $repositoryName $tag -}}
    {{- end -}}
{{- else -}}
    {{- printf "%s/%s:%s" $registryName $repositoryName $tag -}}
{{- end -}}
{{- end -}}


{{/*
Return the proper image name (for the init container service cacert image)
*/}}
{{- define "sampleservice.certinitcontainerimage" -}}
{{- $registryName := .Values.certInitContainers.image.registry | toString  -}}
{{- $repositoryName := .Values.certInitContainers.image.repository | toString  -}}
{{- $tag := .Values.certInitContainers.image.tag | toString -}}

{{/*
Helm 2.11 supports the assignment of a value to a variable defined in a different scope,
but Helm 2.9 and 2.10 doesn't support it, so we need to implement this if-else logic.
Also, we can't use a single if because lazy evaluation is not an option
*/}}
{{- if .Values.global }}
    {{- if .Values.global.imageRegistry }}
        {{- printf "%s/%s:%s" .Values.global.imageRegistry $repositoryName $tag -}}
    {{- else -}}
        {{- printf "%s/%s:%s" $registryName $repositoryName $tag -}}
    {{- end -}}
{{- else -}}
    {{- printf "%s/%s:%s" $registryName $repositoryName $tag -}}
{{- end -}}
{{- end -}}




{{/*
Common labels
*/}}
{{- define "sampleservice.labels" -}}
helm.sh/chart: {{ include "sampleservice.chart" . }}
{{ include "sampleservice.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "sampleservice.selectorLabels" -}}
app.kubernetes.io/name: {{ include "sampleservice.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
k8s-app: {{ include "sampleservice.name" . }}
{{- end -}}


{{/*
Create the name of the service account to use
*/}}
{{- define "sampleservice.serviceAccountName" -}}
{{- if .Values.serviceAccount.create -}}
    {{ default (include "sampleservice.fullname" .) .Values.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{- define "sampleservice.namespace" -}}
{{- default "default" .Values.namespace.nameoverride -}}
{{- end -}}

{{/*
Init Container Volumes
*/}}

{{- define "sampleservice.podspecification" -}}
{{- if or .Values.image.restartPolicy .Values.image.dnsPolicy .Values.image.terminationGracePeriodSeconds }}
restartPolicy: {{ .Values.image.restartPolicy }}
dnsPolicy: {{ .Values.image.dnsPolicy }}
terminationGracePeriodSeconds: {{ .Values.image.terminationGracePeriodSeconds }}
{{ else }}
restartPolicy: Always
dnsPolicy: ClusterFirst
terminationGracePeriodSeconds: 30
{{- end -}}
{{- end -}}

{{- define "sampleservice.deploymentspecification" -}}
{{- if or .Values.progressDeadlineSeconds .Values.revisionHistoryLimit  }}
revisionHistoryLimit: {{ .Values.revisionHistoryLimit }}
progressDeadlineSeconds: {{ .Values.progressDeadlineSeconds }}
{{ else }}
revisionHistoryLimit: 10
progressDeadlineSeconds: 600
{{- end -}}
{{- end -}}



{{- define "sampleservice.azurefilevolume"  -}}
azureFile:
  shareName: {{ .Values.certInitContainers.shareName }}
  secretName: {{  .Values.certInitContainers.secretName  }}
  readOnly: {{ .Values.certInitContainers.readOnly }}
{{- end -}}


{{- define "sampleservice.secretenvironment" -}}
{{- if or $.Values.env $.Values.envSecrets }}
env:
  {{- range $key, $value := $.Values.env }}
  - name: {{ $key }}
    value: {{ $value | quote }}
  {{- end }}
  {{- range $key, $secret := $.Values.envSecrets }}
  - name: {{ $key }}
    valueFrom:
      secretKeyRef:
        name: {{ $secret }}
        key: {{ $key | quote }}
  {{- end }}
{{- end }}
{{- end }}

{{/*
{{- define "sampleservice.environments" -}}
env:
{{- range $key, $value := .Values.env }}
- name: {{ $key }}
  values: {{ $value }}
{{- end }}
{{- end }}
*/}}

{{- define "sampleservice.imagePullSecrets" -}}
{{/*
Helm 2.11 supports the assignment of a value to a variable defined in a different scope,
but Helm 2.9 and 2.10 does not support it, so we need to implement this if-else logic.
Also, we can not use a single if because lazy evaluation is not an option
*/}}
{{- if .Values.global }}
{{- if .Values.global.imagePullSecrets }}
imagePullSecrets:
{{- range .Values.global.imagePullSecrets }}
  - name: {{ . }}
{{- end }}
{{- end }}
{{- else if or .Values.image.pullSecrets  }}
imagePullSecrets:
{{- range .Values.image.pullSecrets }}
  - name: {{ . }}
{{- end }}
{{- end }}
{{- end }}
